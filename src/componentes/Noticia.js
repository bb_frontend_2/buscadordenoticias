import React from 'react';

function Noticia({noticia}) {
    return (
        <div className="col s12 m6  l4">
            <div className="card">
                <div className="card-content">
                    <h3>{noticia.title}</h3>
                </div>
                <div className="card-image">
                    <img src={noticia.urlToImage} alt={noticia.title}></img>
                    <span className="card-title">{noticia.source.name}</span>
                </div>
                <p>{noticia.description}</p>
                <span>{noticia.author}</span>
                <div className="card-action">
                    <a
                        href = {noticia.url}
                        target="_blanck"
                        rel="noopener noreferrer"
                        className="waves-effect waves-light  btn"
                        >
                            Ir a la noticia completa</a>
                </div>
            </div>
        </div>
    );
}

export default Noticia;