import React from 'react';

function Header({titulo}) {
    return (
        <div>
            <nav className="nav-wrapper light-blue darken-3">
                <a href ="#!" className="brand-logo center">{titulo }</a>
            </nav>

        </div>
    );
}

export default Header;