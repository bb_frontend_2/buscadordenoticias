import React from 'react';
import styles from './Formulario.module.css';
import useSelect from '../hooks/useSelect'

function Formulario({guardarCategoria}) {
     // utilizar custom hook

    const OPCIONES = [
         {value: 'general', label:'General'},
         {value: 'business', label:'Negocios'},
         {value: 'entertainment', label:'Entretenimiento'},
         {value: 'health', label:'Salud'},
         {value: 'science', label:'Ciencia'},
         {value: 'sports', label:'Deportes'},
         {value: 'technology', label:'Tecnologuia'},

        ]
    const [categoria, SelectNoticias] = useSelect('general', OPCIONES );
    //submit al form pasar categoria a app.js
        const buscarNoticas = (e)=>{
            e.preventDefault();
            guardarCategoria(categoria);
            console.log("hello")
        }
    return (
        <div className={`${styles.buscador} row`}>
            <div className=" col s12 m8 offset-m2">
                <form
                    onSubmit={buscarNoticas}
                >
                    <h2 className={styles.heading}>encuentra noticias  por categoria</h2>
                    <div className="input-field col  s12">
                        <input
                            type="submit"
                            className={`${styles.btn_block} btn-large amber darken-2`}
                            value="Buscar"
                        />
                        <SelectNoticias/>

                    </div>
                </form>
            </div>

        </div>
    );
}

export default Formulario;