import React, {Fragment, useState, useEffect} from 'react';
import Header from './componentes/Header';
import Formulario from './componentes/Formulario';
import axios from 'axios';
import ListadoNoticias from './componentes/ListadoNoticias';

function App() {

// definir categoria y noticias
const [categoria, guardarCategoria] = useState('')
const [noticias, guardarNoticias] = useState([]);
useEffect(()=>{
  const consultarApi = async()=>{
    const key = 'fd74bcc02c204c0a8c3f2f508298d052';
    const url =`https://newsapi.org/v2/top-headlines?country=co&category=${categoria}&apiKey=${key}`
    const noticias = await axios(url);
    guardarNoticias(noticias.data.articles);


  }
  consultarApi();

}, [categoria]);

  return (
    <Fragment className="App">
      <Header
        titulo="Buscador De Noticias"
      />
      <div className="container white">
        <Formulario
          guardarCategoria = {guardarCategoria}
        />
      </div>
      <ListadoNoticias
        noticias={noticias}
      />
    </Fragment>
  );
}

export default App;
