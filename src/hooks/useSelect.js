import React,{useState} from 'react';

function useSelect(stateInicial, OPCIONES) {
    const [state, actualizarState] = useState(stateInicial);
    console.log(state);
    const SelectNoticias =()=>(
        <select
            className="browser-default"
            value={state}
            onChange={(e)=>actualizarState(e.target.value)}
        >
            <option value="">--Seleccione--</option>
            {
                OPCIONES.map((opcion)=>(
                    <option key={opcion.value} value={opcion.value}>{opcion.label}</option>
                ))
            }
        </select>
    )
    return [state, SelectNoticias];
}

export default useSelect;